#!/usr/bin/env python
import struct, string, math
import argparse, copy

# sys.setrecursionlimit(10000)

#this will be the game object your player will manipulate
class SudokuBoard:

    #the constructor for the SudokuBoard
    def __init__(self, size, board):
      self.BoardSize = size #the size of the board
      self.CurrentGameboard= board #the current state of the game board

    #This function will create a new sudoku board object with
    #with the input value placed on the GameBoard row and col are
    #both zero-indexed
    def set_value(self, row, col, value):
        self.CurrentGameboard[row][col]=value #add the value to the appropriate position on the board
        return SudokuBoard(self.BoardSize, self.CurrentGameboard) #return a new board of the same size with the value added
    


# parse_file
#this function will parse a sudoku text file (like those posted on the website)
#into a BoardSize, and a 2d array [row,col] which holds the value of each cell.
# array elements witha value of 0 are considered to be empty

def parse_file(filename):
    f = open(filename, 'r')
    BoardSize = int( f.readline())
    NumVals = int(f.readline())

    #initialize a blank board
    board= [ [ 0 for i in range(BoardSize) ] for j in range(BoardSize) ]

    #populate the board with initial values
    for i in range(NumVals):
        line = f.readline()
        chars = line.split()
        row = int(chars[0])
        col = int(chars[1])
        val = int(chars[2])
        board[row-1][col-1]=val
    
    return board
    



#takes in an array representing a sudoku board and tests to
#see if it has been filled in correctly
def iscomplete( BoardArray ):
        size = len(BoardArray)
        subsquare = int(math.sqrt(size))

        #check each cell on the board for a 0, or if the value of the cell
        #is present elsewhere within the same row, column, or square
        for row in range(size):
            for col in range(size):

                if BoardArray[row][col]==0:
                    return False
                for i in range(size):
                    if ((BoardArray[row][i] == BoardArray[row][col]) and i != col):
                        return False
                    if ((BoardArray[i][col] == BoardArray[row][col]) and i != row):
                        return False
                #determine which square the cell is in
                SquareRow = row // subsquare
                SquareCol = col // subsquare
                for i in range(subsquare):
                    for j in range(subsquare):
                        if((BoardArray[SquareRow*subsquare + i][SquareCol*subsquare + j] == BoardArray[row][col])
                           and (SquareRow*subsquare + i != row) and (SquareCol*subsquare + j != col)):
                            return False
        return True

#takes in an array representing a sudoku board and tests to
#see if it has been filled in correctly so far
def constraintchecker( BoardArray ):
        global checkcount
        checkcount += 1
        size = len(BoardArray)
        subsquare = int(math.sqrt(size))

        #check each cell on the board for a 0, or if the value of the cell
        #is present elsewhere within the same row, column, or square
        for row in range(size):
            for col in range(size):
                if BoardArray[row][col] == 0:
                    continue

                for i in range(size):
                    if ((BoardArray[row][i] == BoardArray[row][col]) and i != col):
                        return False
                    if ((BoardArray[i][col] == BoardArray[row][col]) and i != row):
                        return False
                #determine which square the cell is in
                SquareRow = row // subsquare
                SquareCol = col // subsquare
                for i in range(subsquare):
                    for j in range(subsquare):
                        if((BoardArray[SquareRow*subsquare + i][SquareCol*subsquare + j] == BoardArray[row][col])
                           and (SquareRow*subsquare + i != row) and (SquareCol*subsquare + j != col)):
                            return False
        #print "row: {} col: {} i: {} val: {}".format(row, col, i, BoardArray[row][col])
        return True


def firstzero(BoardArray):
    size = len(BoardArray)
    for row in range(size):
        for col in range(size):
            if BoardArray[row][col] == 0:
                return [row,col]

# creates a SudokuBoard object initialized with values from a text file like those found on the course website
def init_board( file_name ):
    board = parse_file(file_name)
    return SudokuBoard(len(board), board)

def fwdcheck(sb, rowallow, colallow, subsqallow):
    print "running!"
    if iscomplete(sb.CurrentGameboard):
        print "got it"
        return sb
    var = firstzero(sb.CurrentGameboard)

    domain = range(1,sb.BoardSize + 1)
    subsquare = int(math.sqrt(sb.BoardSize))
    for value in domain:
        if value in rowallow[var[0]] and value in colallow[var[1]] and value in subsqallow[var[0]//subsquare][var[1]//subsquare]:
            print "x: {}, y: {}, Value: {}".format(var[0], var[1], value)
            sb.set_value(var[0], var[1], value)

            new_rowallow = copy.deepcopy(rowallow)
            new_rowallow[var[0]].discard(value)

            new_colallow = copy.deepcopy(colallow)
            new_colallow[var[1]].discard(value)

            new_subsqallow = copy.deepcopy(subsqallow)
            new_subsqallow[var[0]//subsquare][var[1]//subsquare].discard(value)

            return fwdcheck(sb, new_rowallow, new_colallow, new_subsqallow)
    #return fwdcheck(sb, rowallow, colallow, subsqallow)

def forwarwardchecking(sb):
    # Set up allowable values. One array of sets for rows, one for cols, and a matrix of sets for subsquares.
    global rowallow, colallow, subsqallow
    domain = range(1,sb.BoardSize + 1)
    # allowablevalues = [[set(domain) - set([0]) for row in domain] for col in domain]
    # print allowablevalues
    domainset = set(domain)
    cols = zip(*sb.CurrentGameboard) # transpose the board so I can access cols directly
    # rowallow = [domainset - set(row) for row in sb.CurrentGameboard]
    rowallow = []
    colallow = [domainset - set(col) for col in cols]

    subsquare = int(math.sqrt(sb.BoardSize))
    # print subsquare
    subsqallow = [[copy.deepcopy(domainset) for i in range(subsquare)] for j in range(subsquare)]
    # print subsqallow
    for i,row in enumerate(sb.CurrentGameboard):
        rowallow.append(domainset - set(row))
        for j,col in enumerate(row):
            # print str(i//subsquare) + "," + str(j//subsquare) + " " + str(col)
            subsqallow[i//subsquare][j//subsquare].discard(col)
            # print subsqallow[i//subsquare][j//subsquare]
    # print subsqallow
    soln = fwdcheck(sb, rowallow, colallow, subsqallow)
    print soln


def backtracking(sb):
    domain = range(1, sb.BoardSize + 1) # domain of possible values a square can take on for a board of this size
    # print domain
    if iscomplete(sb.CurrentGameboard):
        # print str(sb.CurrentGameboard) + " *"
        print "got it"
        return sb
    var = firstzero(sb.CurrentGameboard)
    # print var
    for value in domain:
        sb.set_value(var[0], var[1], value)
        if constraintchecker(sb.CurrentGameboard):
            # print sb.CurrentGameboard
            backtracking(sb)
            # solution = backtracking(sb)
            # backtracking(sb)
        # else:
        sb.set_value(var[0], var[1], 0)
        # return sb
    return False


# def backtracking(sb):
# backtracking_recursive(sb)


# Read the filename from the commandline and call all the solvers
def main():
    # parser = argparse.ArgumentParser()
    # parser.add_argument("filename", type=str)
    # args = parser.parse_args()

    # filename = args.filename
    filename = "examples/easy/4_4.sudoku"

    sb = init_board(filename)

    # print "initial: " + str(sb.CurrentGameboard)
    for row in sb.CurrentGameboard:
        print row

    # Backtracking
    # soln = backtracking(sb)
    # if soln:
    #     print soln.CurrentGameboard
    # else:
    #     print soln
    # print "{} consistency checks".format(checkcount)
    
    # print sb.CurrentGameboard

    # Forward checking

    forwarwardchecking(sb)

checkcount = 0

if __name__ == "__main__":
    main()
    # print constraintchecker([[0, 2, 0, 0], [0, 0, 0, 2], [0, 3, 4, 0], [4, 0, 0, 0]])
    # print constraintchecker([[8, 6, 2, 4, 9, 5, 3, 0, 7], [0, 9, 7, 6, 0, 3, 0, 2, 0], [0, 3, 4, 0, 2, 8, 0, 1, 0], [4, 0, 0, 9, 0, 0, 7, 0, 5], [1, 0, 0, 0, 0, 0, 0, 0, 4], [3, 0, 9, 0, 0, 6, 0, 0, 2], [0, 8, 0, 3, 4, 0, 5, 6, 0], [0, 4, 0, 5, 0, 9, 8, 0, 0], [6, 0, 0, 0, 1, 0, 2, 0, 3]])
    #print iscomplete([[1, 2, 3, 4], [3, 4, 1, 2], [2, 3, 4, 1], [4, 1, 2, 3]])
    #print iscomplete([[3, 2, 1, 4], [1, 4, 3, 2], [2, 3, 4, 1], [4, 1, 2, 3]])